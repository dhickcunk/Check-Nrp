package com.dhickcunk.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.dhickcunk.helloworld.R.*;

public class MainActivity extends AppCompatActivity {
    private EditText editTextNrp;
    private Button checkButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        checkButton = (Button) findViewById(id.button);
        editTextNrp = (EditText) findViewById(id.nrpText);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editTextNrp.getText().toString().isEmpty()){
                    if (editTextNrp.length() == 9){
                        String nrp = editTextNrp.getText().toString();
                        String kodeJurusan = nrp.substring(2, 5);
                        if (!kodeJurusan.equals("304")){
                            Toast.makeText(MainActivity.this, "You're not a student of Informatics Engineering", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(MainActivity.this, "Your nrp = "+nrp, Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(MainActivity.this, "Harus 9 angka saja", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(MainActivity.this, "Masukan NRP Dulu", Toast.LENGTH_SHORT).show();
                }


            }
        });
        editTextNrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextNrp.selectAll();
            }
        });
    }
}